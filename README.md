# Danbooru-grabber

**健全な生活を送りましょう！__**d=(´▽｀)=b **

**Currently you would have to open the `grabber.py` and use text editor to change line 10 before using**
#### About
This is a simple Python script that downloads pictures from [Danbooru](http://danbooru.donmai.us/). It downloads pictures with tags pre-specified by users and saves the pictures to the folder named after the tags users input. 
**WARNING: You may end up downloading pictures that are NSFW depending on the tags you use.**

#### Prerequisites
- First you need to [have Python installed](https://www.python.org/downloads/). This script is written in Python 2.7.10. It hasn't been tested under Python 3 yet.
- You will also need a Python modules called [requests](http://docs.python-requests.org/en/latest/). 

#### How do I run this thing (on Mac)?
- Open the terminal. 
- Navigate to the folder where you downloaded this script. For example, if you downloaded this script to `/Users/Nico`, then you can type in `cd /Users/Nico` and it will take you there.
- Type in `python grabber.py` and follow the instruction.
- Enjoy!